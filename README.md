# Fizzbuzz

[![pipeline status](https://gitlab.com/adeceneux/fizzbuzz/badges/master/pipeline.svg)](https://gitlab.com/adeceneux/fizzbuzz/commits/master)
&nbsp;&nbsp;&nbsp;&nbsp;
[![coverage report](https://gitlab.com/adeceneux/fizzbuzz/badges/master/coverage.svg)](https://gitlab.com/adeceneux/fizzbuzz/commits/master)

This is REST service that exposes a Fizzbuzz endpoint

## Run from sources

As all the sources are available, you can install it from 
them to test the project locally

To retrieve the sources:

```bash
git clone https://gitlab.com/adeceneux/fizzbuzz.git
```

### From tox

*Requirements*: 
- [python3](https://www.python.org/downloads/)
- [tox](https://tox.readthedocs.io/en/latest/): `pip install tox`

The simplest way to get the project running locally is through the **tox** command. Just run the command:
```bash
tox
``` 

It will start by creating you a virtualenv, activate it, 
then download the required python dependencies and run the REST service inside.

The service will be available on your local port 8080 (http://127.0.0.1:8080/)

### From pip

*Requirements*: 

- [python3](https://www.python.org/downloads/)

If you desire more control over your virtual environment you can install it from pip  
Start by installing the required python dependencies through the command:
```
pip install -rrequirements.txt
```

Then run the service :
```
python -m fizzbuzz
```

### With Docker

Finally, you can also build a local docker image, and run it.
*Requires Docker 17.05 or higher*

```
docker build -t adeceneux/fizzbuzz .
docker run -p 8080:80 adeceneux/fizzbuzz
curl http://127.0.0.1:8080/api/1/fizzbuzz
```

## API Documentation

The API documentation is wrote in YAML following the [OpenAPI (Swagger2) documentation](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md)

Located in the `docs/` folder, you can visualize it with any tool you would like,
however [SwaggerUI](https://swagger.io/tools/swagger-ui/) is recommended.

You can run it locally by starting the service with the following command :
```bash
docker run -p 8081:8080 -e SWAGGER_JSON=/fizzbuzz/swagger.yml -v `pwd`/docs:/fizzbuzz swaggerapi/swagger-ui
```

And access it through your browser at http://127.0.0.1:8081.
This will also allow you to test the API directly from the GUI

An HTML documentation being generated during Docker build, then served at the service root endpoint,
you can access it at `127.0.0.1:8080` if you used a Docker run. 

Otherwise, you can access it directly at the production and development URL.
 - Production server URL: http://35.195.197.187/
 - Development server URL: http://104.155.88.108/

Other tools like [Bootprint](https://github.com/bootprint/bootprint-openapi) may help you generate the HTML file from the documentation

## Folder architecture

```
├── docs/                      # API documentation definition
├── fizzbuzz/                  # Service files
│   ├── algorithm/             # Contains the service algorithms
│   ├── formatters/            # Formatters to serialize models before answering
│   ├── handlers/                   
│   │   ├── endpoints/         # Contains all the class handling an endpoint
│   │   ├── hydrators/         # Contains the hydrators to check and provide the input parameters to the handlers 
│   │   └── main_handler.py    # Parent class of all endpoint handlers                   
│   ├── models/                # Data models
│   ├── storage_contollers/    # Contain the storage controllers to store the default fizzbuzz values
│   ├── __main__.py            # Service entry point
│   ├── config.py              # Define all service configuration variables
│   └── service.py             # Create application and 
├── tests/                     # Unit and functional tests definition
├── .gitlab-ci.yml             # Gitlab CI jobs configuration
├── Dockerfile                 # Docker image build definition
├── kubernetes.tpl.yml         # Kubernetes deployment configuration template - used by CI for deployment
├── requirements.txt           # Python requirements
└── tox.ini                    # Tox configuration
```

## Tests

All tests are located in the `tests/` folder and wrote to be executed with [pytest](https://docs.pytest.org/en/latest/)

If you chose to manually install python dependencies to run the service, you'll need to install the test dependencies as well :

```
pip install -rtests/tests_requirements.txt
```

### Run the tests

To run the whole test suit you can execute the pytest command :

```
pytest
```

If you want to execute a specific suit, or test, here is some example:

```
pytest tests/functionals
pytest tests/functionals/test_fizzbuzz_default_values.py
pytest tests/functionals/test_fizzbuzz_default_values.py::test_fizzbuzz_default_values_nominal_case
```
