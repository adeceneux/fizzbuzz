from tests.functionals import config
from tests.functionals import fizzbuzz_defines
from tests.functionals.client.fizzbuzz_client import FizzbuzzClient

fizzbuzz_client = FizzbuzzClient(config.BASE_URL)


def teardown():
    # Re-set default values at the end of each test
    fizzbuzz_client.put_fizzbuzz(
        replacer1='fizz',
        replacer2='buzz',
        divider1=3,
        divider2=5,
        limit=20
    )


def test_fizzbuzz_default_values_nominal_case():
    """
    Test a call to fizzbuzz PUT endpoint without parameters
    """
    fizzbuzz_client.put_fizzbuzz()

    fizzbuzz_client.assert_204()
    # Check that values are unchanged
    fizzbuzz_client.get_fizzbuzz()
    fizzbuzz_client.assert_200()
    fizzbuzz_client.assert_json_list_is(fizzbuzz_defines.RESULT_DEFAULT_VALUES)


def test_fizzbuzz_change_limit_default_value():
    """
    Test a call to fizzbuzz PUT endpoint to change the limit default value
    """
    fizzbuzz_client.put_fizzbuzz(limit=5)

    fizzbuzz_client.assert_204()
    # Check that values are unchanged
    fizzbuzz_client.get_fizzbuzz()
    fizzbuzz_client.assert_200()
    fizzbuzz_client.assert_json_list_is([1, 2, 'fizz', 4, 'buzz'])


def test_fizzbuzz_change_all_default_values():
    """
    Test a call to fizzbuzz PUT endpoint to change all default values
    """
    fizzbuzz_client.put_fizzbuzz(
        replacer1="str1",
        replacer2="str2",
        divider1=2,
        divider2=4,
        limit=8,
    )

    fizzbuzz_client.assert_204()
    fizzbuzz_client.get_fizzbuzz()
    fizzbuzz_client.assert_200()
    fizzbuzz_client.assert_json_list_is([1, 'str1', 3, 'str1str2', 5, 'str1', 7, 'str1str2'])


def test_fizzbuzz_get_parameter_overrides_default():
    """
    Test a call to fizzbuzz PUT endpoint and check that the GET endpoint parameters
    overrides default values, without changing them
    """
    fizzbuzz_client.put_fizzbuzz(
        replacer1="str1",
        replacer2="str2",
        divider1=2,
        divider2=4,
        limit=8,
    )

    fizzbuzz_client.assert_204()
    fizzbuzz_client.get_fizzbuzz(
        replacer1="foo",
        replacer2="bar",
        divider1=1,
        divider2=1,
        limit=4,
    )
    fizzbuzz_client.assert_200()
    fizzbuzz_client.assert_json_list_is(['foobar', 'foobar', 'foobar', 'foobar'])
    # Check that default values are unchanged
    fizzbuzz_client.get_fizzbuzz()
    fizzbuzz_client.assert_200()
    fizzbuzz_client.assert_json_list_is([1, 'str1', 3, 'str1str2', 5, 'str1', 7, 'str1str2'])


def test_fizzbuzz_default_value_under_minimal_divider1_value():
    """
    Test a call to fizzbuzz PUT endpoint with a too short divider1
    """
    fizzbuzz_client.put_fizzbuzz(divider1=0)

    fizzbuzz_client.assert_400()
    fizzbuzz_client.assert_json_has_items({'error_code': 'invalid_request_parameter'})


def test_fizzbuzz_default_value_under_minimal_divider2_value():
    """
    Test a call to fizzbuzz PUT endpoint with a too short divider2
    """
    fizzbuzz_client.put_fizzbuzz(divider2=0)

    fizzbuzz_client.assert_400()
    fizzbuzz_client.assert_json_has_items({'error_code': 'invalid_request_parameter'})


def test_fizzbuzz_default_value_over_maximal_limit_value():
    """
    Test a call to fizzbuzz PUT endpoint with a too high limit
    """
    fizzbuzz_client.put_fizzbuzz(limit=999999)

    fizzbuzz_client.assert_400()
    fizzbuzz_client.assert_json_has_items({'error_code': 'invalid_request_parameter'})


def test_fizzbuzz_default_value_at_exact_maximal_value():
    """
    Test a call to fizzbuzz PUT endpoint with a too high limit
    """
    fizzbuzz_client.put_fizzbuzz(limit=99999)


def test_fizzbuzz_default_value_with_invalid_json_body():
    """
    Test a call to fizzbuzz PUT endpoint with a too high limit
    """
    fizzbuzz_client.put_fizzbuzz(data="{'this is an invalid json")

    fizzbuzz_client.assert_400()
    fizzbuzz_client.assert_json_has_items({'error_code': 'invalid_request_parameter'})
