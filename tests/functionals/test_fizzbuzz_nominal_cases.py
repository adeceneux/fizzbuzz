from tests.functionals import config
from tests.functionals import fizzbuzz_defines
from tests.functionals.client.fizzbuzz_client import FizzbuzzClient

fizzbuzz_client = FizzbuzzClient(config.BASE_URL)


def test_fizzbuzz_nominal_case():
    """
    Test a call to fizzbuzz GET endpoint with only default values
    """
    fizzbuzz_client.get_fizzbuzz()

    fizzbuzz_client.assert_200()
    fizzbuzz_client.assert_json_list_is(fizzbuzz_defines.RESULT_DEFAULT_VALUES)


def test_fizzbuzz_case_01():
    """
    Test a call to fizzbuzz GET endpoint and update all parameters
    """
    fizzbuzz_client.get_fizzbuzz(
        replacer1='str1', replacer2='str2',
        divider1=2, divider2=4, limit=8
    )

    fizzbuzz_client.assert_200()
    fizzbuzz_client.assert_json_list_is([
        1, "str1", 3, "str1str2", 5, "str1", 7, "str1str2"
    ])


def test_fizzbuzz_under_minimal_divider1_value():
    """
    Test a call to fizzbuzz GET endpoint with a too short divider1
    """
    fizzbuzz_client.get_fizzbuzz(divider1=0)

    fizzbuzz_client.assert_400()
    fizzbuzz_client.assert_json_has_items({'error_code': 'invalid_request_parameter'})


def test_fizzbuzz_under_minimal_divider2_value():
    """
    Test a call to fizzbuzz GET endpoint with a too short divider2
    """
    fizzbuzz_client.get_fizzbuzz(divider2=0)

    fizzbuzz_client.assert_400()
    fizzbuzz_client.assert_json_has_items({'error_code': 'invalid_request_parameter'})


def test_fizzbuzz_over_maximal_limit_value():
    """
    Test a call to fizzbuzz GET endpoint with a too high limit
    """
    fizzbuzz_client.get_fizzbuzz(limit=999999)

    fizzbuzz_client.assert_400()
    fizzbuzz_client.assert_json_has_items({'error_code': 'invalid_request_parameter'})


def test_fizzbuzz_at_exact_maximal_value():
    """
    Test a call to fizzbuzz GET endpoint with a too high limit
    """
    fizzbuzz_client.get_fizzbuzz(limit=99999)
