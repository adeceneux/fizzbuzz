"""
Define some configuration variables for the tests to run
"""
import os

# Service base URL on which execute the tests
BASE_URL = os.getenv('FIZZBUZZ_TESTS_BASE_URL', 'http://127.0.0.1:8080')
