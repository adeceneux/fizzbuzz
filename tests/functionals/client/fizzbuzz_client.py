from tests.functionals.client.base_requests_client import BaseRequestsClient
from tests.functionals.client.client_asserts_mixin import ClientAssertsMixin


class FizzbuzzClient(BaseRequestsClient, ClientAssertsMixin):
    """
    Abstract fizzbuzz API using requests
    """

    def get_fizzbuzz(
            self,
            replacer1: str = None, replacer2: str = None,
            divider1: int = None, divider2: int = None,
            limit: int = None,
            **kwargs
    ):
        """
        Execute the fizzbuzz algorithm and respond the expecting list of number and strings

        Args:
            replacer1 (str): String by which replace every number that can be divided by int1
            replacer2 (str): String by which replace every number that can be divided by int2
            divider1 (int): First number to check
            divider2 (int): Second number to check
            limit (int): Number of entities that must be treated
        """
        # None parameters must not be found in the final request
        query_params = {}
        if replacer1 is not None:
            query_params['string1'] = replacer1
        if replacer2 is not None:
            query_params['string2'] = replacer2
        if divider1 is not None:
            query_params['int1'] = divider1
        if divider2 is not None:
            query_params['int2'] = divider2
        if limit is not None:
            query_params['limit'] = limit
        return self.get(
            '/api/1/fizzbuzz',
            params=query_params,
            **kwargs
        )

    def put_fizzbuzz(
            self,
            replacer1: str = None, replacer2: str = None,
            divider1: int = None, divider2: int = None,
            limit: int = None,
            **kwargs
    ):
        """
        Set and store default values for the fizzbuzz algorithm.

        Args:
            replacer1 (str): String by which replace every number that can be divided by int1
            replacer2 (str): String by which replace every number that can be divided by int2
            divider1 (int): First number to check
            divider2 (int): Second number to check
            limit (int): Number of entities that must be treated
        """
        # None parameters must not be found in the final request
        request_body = {}
        if replacer1 is not None:
            request_body['string1'] = replacer1
        if replacer2 is not None:
            request_body['string2'] = replacer2
        if divider1 is not None:
            request_body['int1'] = divider1
        if divider2 is not None:
            request_body['int2'] = divider2
        if limit is not None:
            request_body['limit'] = limit
        return self.put(
            '/api/1/fizzbuzz',
            json=request_body,
            **kwargs
        )
