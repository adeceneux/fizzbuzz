from requests.sessions import Session


class BaseRequestsClient(object):
    """
    Mixin that provide base methods to execute a request session
    """

    def __init__(self, base_url):
        self.base_url = base_url
        self.last_result = None
        self.session = Session()

    def get(self, endpoint: str, **kwargs):
        """
        Execute a GET request on a specific endpoint

        Args:
             endpoint (str): endpoint to call
        """
        self.last_result = self.session.get(self.base_url + endpoint, **kwargs)
        return self.last_result

    def put(self, endpoint: str, **kwargs):
        """
        Execute a PUT request on a specific endpoint

        Args:
             endpoint (str): endpoint to call
        """
        self.last_result = self.session.put(self.base_url + endpoint, **kwargs)
        return self.last_result
