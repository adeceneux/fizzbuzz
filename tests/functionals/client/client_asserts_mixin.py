class ClientAssertsMixin(object):

    def assert_json_has_items(self, items):
        """
        Assert that the last result returned a JSON containing all the specified items
        """
        last_json = self.last_result.json()
        assert isinstance(last_json, dict), \
            "Error : service response is not a JSON dictionary. Retrieved : %s" % last_json

        for key in items:
            assert key in last_json, "Error : '%s' key was not found in service response" % key
            assert items[key] == last_json[key], \
                "Error : '%s' key has the wrong value in service response. Expected '%s' found '%s'" % (
                    key, items[key], last_json[key]
                )

    def assert_json_list_is(self, expected_list):
        """
        Assert that the last result returned a JSON containing all the specified items
        """
        last_json = self.last_result.json()
        assert isinstance(last_json, list), "Error : service response is not a list. Retrieved : %s" % last_json
        assert len(last_json) == len(expected_list), \
            "Error : service response length should be %s. Retrieved %s" % (len(expected_list), len(last_json))

        for i, value in enumerate(expected_list):
            assert last_json[i] == value, "Error: index %s should be '%s'. Found '%s'" % (
                i, value, last_json[value]
            )

    def assert_list_of_len(self, expected_length: int):
        """
        Assert that the last result returned a JSON list with the right size

        Args:
            expected_length (int): Expected length from the last service response
        """
        last_json = self.last_result.json()
        assert isinstance(last_json, list), "Error : service response is not a list. Retrieved : %s" % last_json
        assert len(last_json) == expected_length, \
            "Error : service response length should be %s. Retrieved %s" % (expected_length, len(last_json))

    def assert_200(self):
        """
        Assert that last result returned a 200 status code
        """
        assert self.last_result.status_code == 200

    def assert_204(self):
        """
        Assert that last result returned a 204 status code
        """
        assert self.last_result.status_code == 204

    def assert_400(self):
        """
        Assert that last result returned a 400 status code
        """
        assert self.last_result.status_code == 400
