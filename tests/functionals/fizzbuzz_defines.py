"""
Define some data used for functional tests
"""

# Expected return of fizzbuzz algorithm with the default values
RESULT_DEFAULT_VALUES = [
    1,
    2,
    "fizz",
    4,
    "buzz",
    "fizz",
    7,
    8,
    "fizz",
    "buzz",
    11,
    "fizz",
    13,
    14,
    "fizzbuzz",
    16,
    17,
    "fizz",
    19,
    "buzz"
]
