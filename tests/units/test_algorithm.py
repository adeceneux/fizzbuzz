"""
Unit tests for the Fizzbuzz algorithm
"""

from fizzbuzz.algorithm.fizzbuzz import execute_fizzbuzz_algorithm


def test_algorithm_with_default_values():
    """
    Test the fizzbuzz algorithm with default values
    """
    result = execute_fizzbuzz_algorithm(
        replacer1='fizz',
        replacer2='buzz',
        divider1=3,
        divider2=5,
        limit=20
    )

    assert result == [
        1, 2, 'fizz', 4, 'buzz',
        'fizz', 7, 8, 'fizz', 'buzz',
        11, 'fizz', 13, 14, 'fizzbuzz',
        16, 17, 'fizz', 19, 'buzz'
    ]


def test_algorithm_has_no_limit():
    """
    Test the fizzbuzz algorithm has no limit itself
    """
    result = execute_fizzbuzz_algorithm(
        replacer1='fizz',
        replacer2='buzz',
        divider1=3,
        divider2=5,
        limit=999999
    )

    assert len(result) == 999999


def test_algorithm_case_01():
    """
    Test the fizzbuzz algorithm case 01
    """
    result = execute_fizzbuzz_algorithm(
        replacer1='fizz',
        replacer2='buzz',
        divider1=1,
        divider2=1,
        limit=5
    )

    assert result == ['fizzbuzz', 'fizzbuzz', 'fizzbuzz', 'fizzbuzz', 'fizzbuzz']


def test_algorithm_case_02():
    """
    Test the fizzbuzz algorithm case 02
    """
    result = execute_fizzbuzz_algorithm(
        replacer1='str1',
        replacer2='str2',
        divider1=1,
        divider2=5,
        limit=4
    )

    assert result == ['str1', 'str1', 'str1', 'str1']


def test_algorithm_case_with_limit_zero():
    """
    Test the fizzbuzz algorithm with a limit set to 0
    """
    result = execute_fizzbuzz_algorithm(
        replacer1='str1',
        replacer2='str2',
        divider1=1,
        divider2=5,
        limit=0
    )

    assert result == []

