import json

from fizzbuzz.handlers.hydrators.hydrator import Hydrator, HydratorError
from fizzbuzz.handlers.hydrators.json import FizzbuzzHydrator
from fizzbuzz.handlers.hydrators.json.fizzbuzz_hydrator import FIZZBUZZ_ENDPOINT_JSON_SCHEMA

fizzbuzz_hydrator = FizzbuzzHydrator()


def test_hydrator_clean_query_parameter_with_the_default_values():
    """
    Send a set of query parameters to the clean_query_parameters function
    returns a well format dictionary, with the fizzbuzz endpoint json schema
    """
    query_parameters = {
        'string1': ['fizz'.encode('utf-8')],
        'string2': ['buzz'.encode('utf-8')],
        'int1': ['3'.encode('utf-8')],
        'int2': ['5'.encode('utf-8')],
        'limit': ['20'.encode('utf-8')]
    }

    result = Hydrator.clean_query_parameters(query_parameters, FIZZBUZZ_ENDPOINT_JSON_SCHEMA)

    expected = {
        'string1': "fizz",
        'string2': "buzz",
        'int1': 3,
        'int2': 5,
        'limit': 20
    }
    assert result == expected


def test_fizzbuzz_hydrator_get_default_values():
    """
    Test fizzbuzz endpoint get hydrator with the default values
    """
    request_arguments = {
        'string1': ['fizz'.encode('utf-8')],
        'string2': ['buzz'.encode('utf-8')],
        'int1': ['3'.encode('utf-8')],
        'int2': ['5'.encode('utf-8')],
        'limit': ['20'.encode('utf-8')]
    }

    result = fizzbuzz_hydrator.get(request_arguments)

    expected = {
        'string1': "fizz",
        'string2': "buzz",
        'int1': 3,
        'int2': 5,
        'limit': 20
    }
    assert result == expected


def test_fizzbuzz_hydrator_without_values():
    """
    Test fizzbuzz endpoint get hydrator without any query parameters
    """
    result = fizzbuzz_hydrator.get(request_arguments={})

    expected = {
        'string1': None,
        'string2': None,
        'int1': None,
        'int2': None,
        'limit': None,
    }
    assert result == expected


def test_fizzbuzz_hydrator_with_only_limit():
    """
    Test fizzbuzz endpoint get hydrator and give only the limit parameter
    """
    request_arguments = {'limit': ['1'.encode('utf-8')]}

    result = fizzbuzz_hydrator.get(request_arguments=request_arguments)

    assert result == {
        'string1': None,
        'string2': None,
        'int1': None,
        'int2': None,
        'limit': 1,
    }


def test_fizzbuzz_hydrator_with_invalid_integer():
    """
    Test fizzbuzz endpoint get hydrator and give a invalid format integer
    """
    request_arguments = {'limit': ['invalid integer'.encode('utf-8')]}

    try:
        fizzbuzz_hydrator.get(request_arguments=request_arguments)
    except HydratorError:
        return

    assert False, "fizzbuzz_hydrator.get didn't raised an exception on a invalid parameter"


def test_fizzbuzz_hydrator_with_a_limit_too_high():
    """
    Test fizzbuzz endpoint get hydrator and give a too high limit
    """
    request_arguments = {'limit': ['999999'.encode('utf-8')]}

    try:
        fizzbuzz_hydrator.get(request_arguments=request_arguments)
    except HydratorError:
        return

    assert False, "fizzbuzz_hydrator.get didn't raised an exception on a invalid parameter"


def test_fizzbuzz_hydrator_with_a_divider_too_low():
    """
    Test fizzbuzz endpoint get hydrator and give a divider (int1) a value under its minimal
    """
    request_arguments = {'int1': ['0'.encode('utf-8')]}

    try:
        fizzbuzz_hydrator.get(request_arguments=request_arguments)
    except HydratorError:
        return

    assert False, "fizzbuzz_hydrator.get didn't raised an exception on a invalid parameter"


def test_fizzbuzz_hydrator_accept_integers_as_replacers():
    """
    Test fizzbuzz endpoint get hydrator allow an integer value to be passed as replacer (string1)
    """
    request_arguments = {'string1': ['42'.encode('utf-8')]}

    result = fizzbuzz_hydrator.get(request_arguments=request_arguments)

    assert result == {
        'string1': '42',
        'string2': None,
        'int1': None,
        'int2': None,
        'limit': None,
    }


def test_fizzbuzz_hydrator_put_default_values():
    """
    Test fizzbuzz endpoint put hydrator with the default values
    """
    json_body = json.dumps({
        'string1': 'fizz',
        'string2': 'buzz',
        'int1': 3,
        'int2': 5,
        'limit': 20
    })

    result = fizzbuzz_hydrator.put(request_body=json_body)

    expected = {
        'string1': "fizz",
        'string2': "buzz",
        'int1': 3,
        'int2': 5,
        'limit': 20
    }
    assert result == expected


def test_fizzbuzz_hydrator_put_without_values():
    """
    Test fizzbuzz endpoint PUT hydrator without any parameters in the json body
    """
    result = fizzbuzz_hydrator.put(request_body='{}')

    expected = {
        'string1': None,
        'string2': None,
        'int1': None,
        'int2': None,
        'limit': None,
    }
    assert result == expected


def test_fizzbuzz_hydrator_put_with_only_limit():
    """
    Test fizzbuzz endpoint PUT hydrator and give only the limit parameter
    """
    json_body = '{"limit": 1}'

    result = fizzbuzz_hydrator.put(request_body=json_body)

    assert result == {
        'string1': None,
        'string2': None,
        'int1': None,
        'int2': None,
        'limit': 1,
    }


def test_fizzbuzz_hydrator_put_with_invalid_integer():
    """
    Test fizzbuzz endpoint PUT hydrator and give a invalid format integer
    """
    json_body = '{"limit": "invalid integer"}'

    try:
        fizzbuzz_hydrator.put(request_body=json_body)
    except HydratorError:
        return

    assert False, "fizzbuzz_hydrator.put didn't raised an exception on a invalid parameter"


def test_fizzbuzz_hydrator_put_with_a_limit_too_high():
    """
    Test fizzbuzz endpoint PUT hydrator and give a too high limit
    """
    json_body = '{"limit": 999999}'

    try:
        fizzbuzz_hydrator.put(request_body=json_body)
    except HydratorError:
        return

    assert False, "fizzbuzz_hydrator.put didn't raised an exception on a invalid parameter"


def test_fizzbuzz_hydrator_put_with_a_divider_too_low():
    """
    Test fizzbuzz endpoint PUT hydrator and give a divider (int1) a value under its minimal
    """
    json_body = '{"int1": 0}'

    try:
        fizzbuzz_hydrator.put(request_body=json_body)
    except HydratorError:
        return

    assert False, "fizzbuzz_hydrator.put didn't raised an exception on a invalid parameter"


def test_fizzbuzz_hydrator_put_with_an_invalid_json_body():
    """
    Test fizzbuzz endpoint PUT hydrator with an invalid JSON as payload
    """
    invalid_json_body = 'This is not a JSON'

    try:
        fizzbuzz_hydrator.put(request_body=invalid_json_body)
    except HydratorError:
        return

    assert False, "fizzbuzz_hydrator.put didn't raised an exception on a invalid parameter"
