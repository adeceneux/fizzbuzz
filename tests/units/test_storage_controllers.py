from fizzbuzz.storage_controllers.in_memory_storage import InMemoryStorage


def test_in_memory_storage_default_values():
    """
    Test that InMemoryStorage provides default values
    """
    storage = InMemoryStorage()

    result = storage.get_fizzbuzz_default_values()

    assert result == {
        'replacer1': 'fizz',
        'replacer2': 'buzz',
        'divider1': 3,
        'divider2': 5,
        'limit': 20,
    }


def test_in_memory_replace_default_values():
    """
    Test that InMemoryStorage can replace all default values at once
    """
    storage = InMemoryStorage()

    storage.set_fizzbuzz_default_values(
        replacer1='default_1',
        replacer2='default_2',
        divider1=-5,
        divider2=15,
        limit=50
    )

    result = storage.get_fizzbuzz_default_values()
    assert result == {
        'replacer1': 'default_1',
        'replacer2': 'default_2',
        'divider1': -5,
        'divider2': 15,
        'limit': 50,
    }


def test_in_memory_replace_only_some_default_values():
    """
    Test that InMemoryStorage can replace only the non-None default values and keep the others unchanged
    """
    storage = InMemoryStorage()

    storage.set_fizzbuzz_default_values(
        replacer1=None,
        replacer2='default_2',
        divider1=-5,
        divider2=None,
        limit=50
    )

    result = storage.get_fizzbuzz_default_values()
    assert result == {
        'replacer1': 'fizz',
        'replacer2': 'default_2',
        'divider1': -5,
        'divider2': 5,
        'limit': 50,
    }
