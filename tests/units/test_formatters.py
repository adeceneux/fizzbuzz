import json

from fizzbuzz.formatters.json_formatters import JSONFormatter
from fizzbuzz.models.fizzbuzz_result import FizzbuzzResult
from fizzbuzz.models.http_error import HttpError
from fizzbuzz.models.model import Model

json_formatter = JSONFormatter()


def test_json_format_fizzbuzz_result():
    """
    Test to serialize a FizzbuzzResult object through the JSON formatter
    """
    entries = [1, 2, 'fizz', 4, 'buzz', 6]
    fizzbuzz_result = FizzbuzzResult(entries)

    json_result = json_formatter.format(fizzbuzz_result)

    json_result_decoded = json.loads(json_result)
    assert json_result_decoded == entries


def test_json_format_empty_fizzbuzz_result():
    """
    Test to serialize a FizzbuzzResult object through the JSON formatter
    """
    fizzbuzz_result = FizzbuzzResult()

    json_result = json_formatter.format(fizzbuzz_result)

    assert json_result == "[]"


def test_retrieve_json_formatter_web_content_type():
    """
    Test to retrieve the web content type of a JSON formatter
    """
    assert json_formatter.get_web_content_type()


def test_json_format_big_fizzbuzz_result():
    """
    Test to serialize a FizzbuzzResult object with a big entry list
    """
    entries = [0] * 99999
    fizzbuzz_result = FizzbuzzResult(entries)

    json_result = json_formatter.format(fizzbuzz_result)

    json_result_decoded = json.loads(json_result)
    assert json_result_decoded == entries


def test_json_format_http_error():
    """
    Test to serialize a HttpError object through a JSON formatter
    """
    http_error = HttpError(error_code="code_1", message="message_1")

    json_result = json_formatter.format(http_error)

    assert json_result == '{"error_code": "code_1", "message": "message_1"}'


def test_json_format_with_no_message():
    """
    Test to serialize a HttpError object with a message set to None
    """
    http_error = HttpError(error_code="code_1", message=None)

    json_result = json_formatter.format(http_error)

    assert json_result == '{"error_code": "code_1"}'


def test_json_format_with_no_parameters():
    """
    Test to serialize a HttpError object with both parameters set to None
    """
    http_error = HttpError(error_code=None, message=None)

    json_result = json_formatter.format(http_error)

    assert json_result == '{"error_code": null}'


def test_json_format_wrong_object():
    """
    Test to format a model that the json formatter can't serialize
    """
    # Define a model that the json formatter can't use
    class WrongModel(Model):
        def get_type(self):
            return None

    try:
        json_formatter.format(WrongModel())
    except TypeError:
        return

    assert False, 'The json formatter allowed a invalid model to be serialized'
