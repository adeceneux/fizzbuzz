import datetime

from tornado import gen
from tornado import util
from tornado.ioloop import IOLoop

from fizzbuzz.algorithm.fizzbuzz import execute_fizzbuzz_algorithm
from fizzbuzz.handlers.main_handler import Handler
from fizzbuzz.handlers.hydrators.hydrator import hydrate
from fizzbuzz.models.fizzbuzz_result import FizzbuzzResult


class FizzbuzzEndpoint(Handler):
    """
    Handle the fizzbuzz endpoint
    """

    @hydrate
    @gen.coroutine
    def get(
            self,
            string1: str, string2: str,
            int1: int, int2: int,
            limit: int, **_
    ):
        """
        Execute the fizzbuzz algorithm and respond the expecting list of number and strings

        Args:
            string1 (str): String by which replace every number that can be divided by int1
            string2 (str): String by which replace every number that can be divided by int2
            int1 (int): First number to check
            int2 (int): Second number to check
            limit (int): Number of entities that must be treated
        """
        # Retrieve default values
        default_value = self.storage.get_fizzbuzz_default_values()
        # Use retrieved parameters for the Fizzbuzz algorithm, or their default value if none
        fizzbuzz_parameters = [
            string1 if string1 is not None else default_value.get('replacer1'),
            string2 if string2 is not None else default_value.get('replacer2'),
            int1 if int1 is not None else default_value.get('divider1'),
            int2 if int2 is not None else default_value.get('divider2'),
            limit if limit is not None else default_value.get('limit'),
        ]

        future = IOLoop.current().run_in_executor(
            self.endpoint_thread_pool,
            execute_fizzbuzz_algorithm,
            *fizzbuzz_parameters
        )
        try:
            result = yield gen.with_timeout(
                datetime.timedelta(seconds=0.1),
                future
            )
        except util.TimeoutError:
            future.cancel()
            self.response_timeout(message="The server couldn't respond in time. Try lower the limit specified")
            return

        self.response_success(FizzbuzzResult(result))

    @hydrate
    def put(
            self,
            string1: str, string2: str,
            int1: int, int2: int,
            limit: int, **_
    ):
        """
        Set and store default values for the fizzbuzz algorithm.

        Args:
            string1 (str): String by which replace every number that can be divided by int1
            string2 (str): String by which replace every number that can be divided by int2
            int1 (int): First number to check
            int2 (int): Second number to check
            limit (int): Number of entities that must be treated
        """
        self.storage.set_fizzbuzz_default_values(
            replacer1=string1, replacer2=string2,
            divider1=int1, divider2=int2,
            limit=limit,
        )
        self.response_no_content()
