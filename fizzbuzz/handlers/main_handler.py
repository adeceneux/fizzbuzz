from concurrent import futures

from tornado.web import RequestHandler

from fizzbuzz import config
from fizzbuzz.models.model import Model
from fizzbuzz.models.http_error import HttpError
from fizzbuzz.formatters.main_formatter import Formatter
from fizzbuzz.storage_controllers.storage import Storage


class Handler(RequestHandler):
    """
    Parent class of any endpoint
    """

    def __init__(self, *args, **kwargs):
        self.formatter = None
        self.hydrator = None
        self.storage = None
        self.endpoint_thread_pool = futures.ThreadPoolExecutor(config.ALLOWED_THREAD_PER_ENDPOINT)
        super().__init__(*args, **kwargs)

    def initialize(
            self,
            formatter: Formatter = None,
            hydrator: callable = None,
            storage: Storage = None,
            **kwargs):
        """
        Retrieve formatter and hydrator from the additional parameters if any

        Args:
            formatter (Formatter): Formatter used to format all endpoint responses
            hydrator (Hydrator): Hydrator used to hydrate all endpoint parameters
            storage (Storage): Storage controller to use to set/retrieve any stored object
        """
        self.formatter = formatter
        self.hydrator = hydrator
        self.storage = storage
        self.add_cors_headers()

    def add_cors_headers(self):
        self.add_header('Access-Control-Allow-Origin', config.CORS_ORIGINS)
        self.add_header('Access-Control-Allow-Methods', config.CORS_ORIGINS)
        self.add_header('Access-Control-Allow-Headers', config.CORS_HEADERS)

    def data_received(self, chunk):
        """
        Treat a chunk as soon as received
        Not used, but we have to override the `RequestHandler`'s one
        """
        pass

    def options(self, *args, **kwargs):
        """
        Handle OPTIONS verb
        """
        # Do nothing, just return success for web browsers
        pass

    def get(self, *args, **kwargs):
        """
        Handle GET verb
        """
        # Return 405 if the method is not override by the child
        self.response_method_not_allowed()

    def post(self, *args, **kwargs):
        """
        Handle POST verb
        """
        # Return 405 if the method is not override by the child
        self.response_method_not_allowed()

    def put(self, *args, **kwargs):
        """
        Handle PUT verb
        """
        # Return 405 if the method is not override by the child
        self.response_method_not_allowed()

    def delete(self, *args, **kwargs):
        """
        Handle DELETE verb
        """
        # Return 405 if the method is not override by the child
        self.response_method_not_allowed()

    def patch(self, *args, **kwargs):
        """
        Handle PATCH verb
        """
        # Return 405 if the method is not override by the child
        self.response_method_not_allowed()

    def response_success(self, model: Model = None):
        """
        Return a successful response and use the handler formatter
        to build the response content
        """
        self.add_header('Content-Type', self.formatter.get_web_content_type())
        if model is not None:
            self.write(self.formatter.format(model))

    def response_no_content(self):
        """
        Return a successful response without any body content
        """
        self.set_status(204)

    def response_bad_parameter(
            self,
            error_code: str = 'bad_parameter', message: str = None
    ):
        """
        Respond a 400 error code

        Args:
            error_code (str): Error code to return in the response payload
            message (str): Additional message that can be passed among the payload
        """
        self.response_error(400, error_code, message)

    def response_method_not_allowed(
            self,
            error_code: str = 'method_not_allowed', message: str = None
    ):
        """
        Respond a 405 error code

        Args:
            error_code (str): Error code to return in the response payload
            message (str): Additional message that can be passed among the payload
        """
        self.response_error(405, error_code, message)

    def response_timeout(
            self,
            error_code: str = 'timeout', message: str = None
    ):
        """
        Return a 408 error code

        Args:
            error_code (str): Error code to return in the response payload
            message (str): Additional message that can be passed among the payload
        """
        self.response_error(408, error_code, message)

    def response_error(
            self,
            status_code: int, error_code: str, message: str
    ):
        """
        Return an error to the client and finish the request

        Args:
            status_code (int): HTTP code that must be returned
            error_code (str): Error code to return in the response payload
            message (str): Additional message that can be passed among the payload
        """
        self.clear()
        # Re-add the cors headers after a clean
        self.add_cors_headers()
        self.add_header('Content-Type', self.formatter.get_web_content_type())
        self.set_status(status_code)

        http_error = HttpError(error_code, message)
        self.finish(self.formatter.format(http_error))
