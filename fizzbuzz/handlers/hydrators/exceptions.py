"""
Definitions of all project exceptions that can be raised
"""


class ServiceException(Exception):
    """
    Parent class of all project exception
    """
    def __init__(self, message: str):
        self.message = message


class HydratorError(ServiceException):
    """
    An error occurred during an hydration
    """
    pass
