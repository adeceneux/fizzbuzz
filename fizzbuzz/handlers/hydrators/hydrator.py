from fizzbuzz.handlers.main_handler import Handler
from fizzbuzz.handlers.hydrators.exceptions import HydratorError


def hydrate(func):
    """
    Decorator used to hydrate parameters of an endpoint
    """

    def wrapper(handler: Handler, *args, **kwargs) -> callable:
        try:
            # Retrieve the required parameters using the endpoint hydrator
            additional_arguments = handler.hydrator.hydrate_from_http_verb(
                verb=handler.request.method,
                request_body=handler.request.body.decode('utf-8'),
                request_arguments=handler.request.arguments,
            )
        except HydratorError as e:
            # Invalid payload
            handler.response_bad_parameter('invalid_request_parameter', e.message)
            return

        # Add the retrieved parameters among with the kwargs
        return func(handler, *args, **{**kwargs, **additional_arguments})

    return wrapper


class Hydrator(object):
    """
    Parent class of any hydrator
    """

    def __init__(self):
        self.HYDRATOR_VERB_MAP = {
            'GET': self.get,
            'POST': self.post,
            'PUT': self.put,
            'DELETE': self.delete,
            'PATCH': self.patch,
        }

    @staticmethod
    def clean_query_parameters(request_arguments: dict, schema: dict) -> dict:
        """
        Clean tornado provided query parameters
        As Tornado stores each query parameter in list

        Args:
            request_arguments (dict): Tornado query parameters
            schema (dict): Schema used to use the right cast if needed

        Return:
            dict: clean dictionary containing all request query parameters
        """
        clean_request_arguments = dict()
        for key in request_arguments:
            value = request_arguments[key][0].decode('utf-8')
            if (
                    value.isdigit() and
                    key in schema['properties'].keys() and
                    schema['properties'][key].get('type') == "integer"
            ):
                # Convert in integer if parameters only contains numbers
                value = int(value)
            clean_request_arguments[key] = value
        return clean_request_arguments

    def hydrate_from_http_verb(
            self, verb: str,
            request_body: str, request_arguments: dict
    ) -> dict:
        """
        Hydrate an endpoint parameters from an HTTP verb

        Args:
            verb (str): HTTP verb to call
            request_body (str): Body of the request to parse
            request_arguments (dict): Received query parameters

        Returns:
            dict: Parsed arguments to inject as named parameters
        """
        hydrator_method = self.HYDRATOR_VERB_MAP.get(verb)
        if hydrator_method is None:
            raise NotImplemented("Can't hydrate verb %s. Not implemented" % verb)

        return hydrator_method(request_body=request_body, request_arguments=request_arguments)

    @staticmethod
    def get(request_body: str, request_arguments: dict) -> dict:
        """
        Hydrate parameters for the endpoint GET verb.
        Must be overload by a child class

        Args:
            request_body (str): Body of the request to parse
            request_arguments (dict): Received query parameters

        Returns:
            dict: Parsed arguments to inject as named parameters
        """
        raise NotImplemented('Hydrator.get has not been properly overload')

    @staticmethod
    def post(request_body: str, request_arguments: dict) -> dict:
        """
        Hydrate parameters for the endpoint POST verb.
        Must be overload by a child class

        Args:
            request_body (str): Body of the request to parse
            request_arguments (dict): Received query parameters

        Returns:
            dict: Parsed arguments to inject as named parameters
        """
        raise NotImplemented('Hydrator.post has not been properly overload')

    @staticmethod
    def put(request_body: str, request_arguments: dict) -> dict:
        """
        Hydrate parameters for the endpoint PUT verb.
        Must be overload by a child class

        Args:
            request_body (str): Body of the request to parse
            request_arguments (dict): Received query parameters

        Returns:
            dict: Parsed arguments to inject as named parameters
        """
        raise NotImplemented('Hydrator.put has not been properly overload')

    @staticmethod
    def delete(request_body: str, request_arguments: dict) -> dict:
        """
        Hydrate parameters for the endpoint DELETE verb.
        Must be overload by a child class

        Args:
            request_body (str): Body of the request to parse
            request_arguments (dict): Received query parameters

        Returns:
            dict: Parsed arguments to inject as named parameters
        """
        raise NotImplemented('Hydrator.delete has not been properly overload')

    @staticmethod
    def patch(request_body: str, request_arguments: dict) -> dict:
        """
        Hydrate parameters for the endpoint PATCH verb.
        Must be overload by a child class

        Args:
            request_body (str): Body of the request to parse
            request_arguments (dict): Received query parameters

        Returns:
            dict: Parsed arguments to inject as named parameters
        """
        raise NotImplemented('Hydrator.patch has not been properly overload')
