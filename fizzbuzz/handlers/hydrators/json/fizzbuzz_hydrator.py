import json
import jsonschema

from fizzbuzz.handlers.hydrators.hydrator import Hydrator
from fizzbuzz.handlers.hydrators.exceptions import HydratorError

FIZZBUZZ_ENDPOINT_JSON_SCHEMA = {
    "type": "object",
    "properties": {
        "string1": {"type": "string"},
        "string2": {"type": "string"},
        "int1": {
            "type": "integer",
            "minimum": 1,
            "maximum": 999999999999999
        },
        "int2": {
            "type": "integer",
            "minimum": 1,
            "maximum": 999999999999999
        },
        "limit": {
            "type": "integer",
            "minimum": 0,
            "maximum": 99999
        }
    }
}


class FizzbuzzHydrator(Hydrator):

    @staticmethod
    def get(request_arguments: dict, **_) -> dict:
        """
        Hydrate the parameters for the fizzbuzz endpoint GET method
        and validate the given parameters

        Args:
            request_arguments (dict): Received query parameters

        Returns:
             dict: parsed query parameters with all missing parameters set to None
        """
        request_arguments = Hydrator.clean_query_parameters(request_arguments, FIZZBUZZ_ENDPOINT_JSON_SCHEMA)
        try:
            # Validate the schema
            jsonschema.validate(request_arguments, FIZZBUZZ_ENDPOINT_JSON_SCHEMA)
        except jsonschema.ValidationError as e:
            raise HydratorError(e.message)

        return {
            'string1': request_arguments.get('string1'),
            'string2': request_arguments.get('string2'),
            'int1': request_arguments.get('int1'),
            'int2': request_arguments.get('int2'),
            'limit': request_arguments.get('limit'),
        }

    @staticmethod
    def put(request_body: str, **_) -> dict:
        """
        Hydrate the parameters for the fizzbuzz endpoint PUT method
        and validate the payload

        Args:
            request_body (str): Body of the request to parse

        Returns:
             dict: parsed JSON body with all missing parameters set to None
        """
        try:
            # Parse JSON body
            json_body = json.loads(request_body)
        except json.decoder.JSONDecodeError as e:
            raise HydratorError(e.msg)

        try:
            # Validate the schema
            jsonschema.validate(json_body, FIZZBUZZ_ENDPOINT_JSON_SCHEMA)
        except jsonschema.ValidationError as e:
            raise HydratorError(e.message)

        return {
            'string1': json_body.get('string1'),
            'string2': json_body.get('string2'),
            'int1': json_body.get('int1'),
            'int2': json_body.get('int2'),
            'limit': json_body.get('limit'),
        }
