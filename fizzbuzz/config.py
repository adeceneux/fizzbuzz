"""
Group all the service settings
"""
import os

# The port the service must listen to
SERVICE_PORT = os.getenv('FIZZBUZZ_SERVICE_PORT', '8080')
DEBUG_MODE = os.getenv('FIZZBUZZ_DEBUG_MODE', 'false') == 'true'

# CORS headers definition
CORS_ORIGINS = os.getenv('FIZZBUZZ_CORS_ORIGIN', '*')
CORS_METHODS = os.getenv('FIZZBUZZ_CORS_METHODS', '*')
CORS_HEADERS = os.getenv('FIZZBUZZ_CORS_HEADERS', '*')

# Path to the static files to deliver on root
DOC_DIRECTORY_PATH = os.getenv('FIZZBUZZ_DOC_DIRECTORY_PATH', 'fizzbuzz/docs/')

# Define the number of thread allowed in thread pools per endpoint
ALLOWED_THREAD_PER_ENDPOINT = int(os.getenv('FIZZBUZZ_ALLOWED_THREAD_PER_ENDPOINT', '3'))
