from fizzbuzz.models.model import Model


class Formatter(object):

    @staticmethod
    def format(model: Model) -> str:
        """
        Format a model
        """
        raise NotImplemented('format has not been properly overloaded')

    @staticmethod
    def get_web_content_type() -> str:
        """
        Return the content type that should be used in HTTP headers for this formatter
        """
        raise NotImplemented('get_web_content_type has not been properly overloaded')
