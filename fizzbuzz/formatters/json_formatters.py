import json

from fizzbuzz.formatters.main_formatter import Formatter
from fizzbuzz.models.model import ModelType, Model
from fizzbuzz.models.http_error import HttpError
from fizzbuzz.models.fizzbuzz_result import FizzbuzzResult


class JSONFormatter(Formatter):
    """
    Formatter used to convert any model in JSON
    """

    @staticmethod
    def format(model: Model) -> str:
        """
        Format a model in JSON

        Args:
            model (Model): Model to format

        Returns:
            str: JSON serialized string representing the model
        """
        authorized_model_types = {
            ModelType.FIZZBUZZ_RESULT: JSONFormatter._format_fizzbuzz_result,
            ModelType.HTTP_ERROR: JSONFormatter._format_http_error,
        }
        if model.get_type() not in authorized_model_types:
            raise TypeError(
                'Model type %s is not authorized by the JSONFormatter' % model.get_type()
            )

        return authorized_model_types[model.get_type()](model)

    @staticmethod
    def _format_fizzbuzz_result(fizzbuzz_result: FizzbuzzResult) -> str:
        """
        Format a fizzbuzz result model in JSON

        Args:
            fizzbuzz_result (FizzbuzzResult): Model to format

        Returns:
            str: JSON serialized string representing the model
        """
        return json.dumps(fizzbuzz_result.get_entries())

    @staticmethod
    def _format_http_error(http_error: HttpError) -> str:
        """
        Format an HTTP error model in JSON

        Args:
             http_error (HTTPError): Model to format

        Returns:
            str: JSON serialized string representing the model
        """
        response = {'error_code': http_error.get_error_code()}
        if http_error.get_message() is not None:
            response['message'] = http_error.get_message()
        return json.dumps(response)

    @staticmethod
    def get_web_content_type() -> str:
        """
        Return the content type that should be used in HTTP headers for this formatter
        """
        return 'application/json'
