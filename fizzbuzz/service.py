import tornado.web

from fizzbuzz import config
from fizzbuzz.handlers.hydrators import json
from fizzbuzz.formatters.json_formatters import JSONFormatter
from fizzbuzz.handlers.endpoints.fizzbuzz import FizzbuzzEndpoint
from fizzbuzz.storage_controllers.in_memory_storage import InMemoryStorage

DEFAULT_ENDPOINT_PARAMETERS = dict(
    formatter=JSONFormatter,
    hydrator=json.FIZZBUZZ,
    storage=InMemoryStorage(),
)


def make_app() -> tornado.web.Application:
    """
    Configure the Tornado application and define the endpoints

    Returns:
        tornado.web.Application: Configured Tornado application
    """
    return tornado.web.Application([
        (r"/api/1/fizzbuzz", FizzbuzzEndpoint, DEFAULT_ENDPOINT_PARAMETERS),
        (
            r"/(.*)", tornado.web.StaticFileHandler,
            {'path': config.DOC_DIRECTORY_PATH, 'default_filename': 'index.html'}
        ),
    ])
