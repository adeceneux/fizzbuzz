"""
Algorithms for the fizzbuzz endpoint
"""


def execute_fizzbuzz_algorithm(
        replacer1: str, replacer2: str,
        divider1: int, divider2: int,
        limit: int
) -> list:
    """
    Execute the Fizzbuzz algorithm
    Replace in a list of number from 1 to limit two strings,
    if the number can be divided by int1 or int2

    Args:
         replacer1 (str): String by which replace every number that can be divided by int1
         replacer2 (str): String by which replace every number that can be divided by int2
         divider1 (int): First number to check
         divider2 (int): Second number to check
         limit (int): Number of entities that must be treated

    Return:
        list: List of numbers or strings resulting resulting from the algorithm
    """
    entries = []
    for i in range(1, limit + 1):
        entry = ""
        if i % divider1 == 0:
            entry = replacer1
        if i % divider2 == 0:
            entry += replacer2
        entries.append(entry if len(entry) else i)
    return entries
