from fizzbuzz.storage_controllers.storage import Storage


class InMemoryStorage(Storage):
    """
    Parent class for any storage controller
    """

    def __init__(self):
        self._replacer1 = "fizz"
        self._replacer2 = "buzz"
        self._divider1 = 3
        self._divider2 = 5
        self._limit = 20

    def get_fizzbuzz_default_values(self) -> dict:
        """
        Retrieve fizzbuzz default values if any.
        Must be overload by any child class

        Returns:
            dict: A map of all fizzbuzz algorithm default values
        """
        return {
            'replacer1': self._replacer1,
            'replacer2': self._replacer2,
            'divider1': self._divider1,
            'divider2': self._divider2,
            'limit': self._limit,
        }

    def set_fizzbuzz_default_values(
            self,
            replacer1: str, replacer2: str,
            divider1: int, divider2: int,
            limit: int
    ):
        """
        Set fizzbuzz default values for the Fizzbuzz algorithm.
        Any None parameter will be ignored
        Must be overload by any child class

        Args:
            replacer1 (str): String by which replace every number that can be divided by int1
            replacer2 (str): String by which replace every number that can be divided by int2
            divider1 (int): First number to check
            divider2 (int): Second number to check
            limit (int): Number of entities that must be treated
        """
        if replacer1 is not None:
            self._replacer1 = replacer1
        if replacer2 is not None:
            self._replacer2 = replacer2
        if divider1 is not None:
            self._divider1 = divider1
        if divider2 is not None:
            self._divider2 = divider2
        if limit is not None:
            self._limit = limit
