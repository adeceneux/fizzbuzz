class Storage(object):
    """
    Parent class for any storage controller
    """

    def get_fizzbuzz_default_values(self) -> dict:
        """
        Retrieve fizzbuzz default values if any.
        Must be overload by any child class

        Returns:
            dict: A map of all fizzbuzz algorithm default values
        """
        raise NotImplemented('Storage.get_fizzbuzz_default_values has not been properly overload')

    def set_fizzbuzz_default_values(
            self,
            replacer1: str, replacer2: str,
            divider1: int, divider2: int,
            limit: int
    ):
        """
        Set fizzbuzz default values for the Fizzbuzz algorithm.
        Must be overload by any child class

        Args:
            replacer1 (str): String by which replace every number that can be divided by int1
            replacer2 (str): String by which replace every number that can be divided by int2
            divider1 (int): First number to check
            divider2 (int): Second number to check
            limit (int): Number of entities that must be treated
        """
        raise NotImplemented('Storage.set_fizzbuzz_default_values has not been properly overload')
