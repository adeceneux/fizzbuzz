from fizzbuzz.models.model import Model, ModelType


class FizzbuzzResult(Model):
    """
    Model representing a Fizzbuzz algorithm result
    """

    def __init__(self, entries: list = None):
        self._entries = entries
        if self._entries is None:
            self._entries = []

    def get_entries(self) -> list:
        """
        Returns the list of entries
        """
        return self._entries

    def get_type(self) -> ModelType:
        """
        Returns the corresponding type of Model

        Returns:
            ModelType: Type of model
        """
        return ModelType.FIZZBUZZ_RESULT
