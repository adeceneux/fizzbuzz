from enum import Enum


class ModelType(Enum):
    """
    Known type of model
    """
    FIZZBUZZ_RESULT = 1
    HTTP_ERROR = 2


class Model(object):
    """
    Parent class of any model
    """

    def get_type(self) -> ModelType:
        """
        Returns the type of the model
        Must be override by the child
        """
        raise NotImplemented("get_type has not been properly overloaded")
