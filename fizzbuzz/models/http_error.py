from fizzbuzz.models.model import Model, ModelType


class HttpError(Model):
    """
    Model representing an HTTP error
    """

    def __init__(self, error_code: str, message: str):
        self._error_code = error_code
        self._message = message

    def get_error_code(self) -> str:
        """
        Returns the error code
        """
        return self._error_code

    def get_message(self) -> str:
        """
        Returns the additional message
        """
        return self._message

    def get_type(self) -> ModelType:
        """
        Returns the corresponding type of Model

        Returns:
            ModelType: Type of model
        """
        return ModelType.HTTP_ERROR
