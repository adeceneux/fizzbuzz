import signal

from tornado.ioloop import IOLoop

from fizzbuzz import config
from fizzbuzz.service import make_app


def signal_handler():
    """
    Handle SIGINT signal to shutdown the service gracefully
    """
    IOLoop.current().stop()
    print("Service stopped")


if __name__ == "__main__":
    app = make_app()
    io_loop = IOLoop.instance()
    signal.signal(signal.SIGINT, lambda signum, frame: io_loop.add_callback_from_signal(signal_handler))
    http_server = app.listen(config.SERVICE_PORT)
    print("Service Fizzbuzz launched on port %s" % config.SERVICE_PORT)
    io_loop.start()
