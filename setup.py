from distutils.core import setup

setup(
    name='Fizzbuzz',
    version='1.0',
    description='Fizzbuzz REST service example',
    author='Alexandre Deceneux',
    url='https://gitlab.com/adeceneux/fizzbuzz',
    packages=['fizzbuzz'],
)
