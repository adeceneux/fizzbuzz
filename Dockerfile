# Build Swagger documentation
FROM node:8-alpine as doc_build
RUN npm install -g bootprint bootprint-openapi
COPY docs docs
RUN bootprint openapi docs/swagger.yml /output_docs

# Copy service files and install python requirements
MAINTAINER Alexandre Deceneux
FROM python:3.7-alpine3.7
ADD requirements.txt ./
RUN pip install -rrequirements.txt
ADD fizzbuzz /fizzbuzz
COPY --from=doc_build /output_docs /fizzbuzz/docs

ENV FIZZBUZZ_SERVICE_PORT 80
ENV FIZZBUZZ_DEBUG_MODE "false"
EXPOSE 80

CMD /usr/local/bin/python -m fizzbuzz
